<?php

namespace app\controllers;

use Yii;

use yii\web\Controller;


class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionNumeros()
    {
        //creo un array con numeros 
        $numeros=[1,2,3,4,34,23,12];
        return $this->render('listar',[
            "datos"=>$numeros
        ]);
    }

    public function actionLetras()
    {
        $letras=["a","c","h","j"];
        return $this->render('listar',[
            "datos"=>$letras
        ]);
    }
    
}
