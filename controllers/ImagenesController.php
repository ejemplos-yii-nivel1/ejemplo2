<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;

class ImagenesController extends Controller {

    public function init(){
        parent::init();
        $this->layout="imagenes";
    }
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex() {
        /* crear un array de las fotos y las descripciones. */
        $imagenes = [
            [
                "nombre" => "1.jpg",
                "descripcion" => "Primera foto",
            ],
            [
                "nombre" => "2.jpg",
                "descripcion" => "Segunda foto",
            ],
            [
                "nombre" => "1.jpg",
                "descripcion" => "Primera foto",
            ],
            [
                "nombre" => "2.jpg",
                "descripcion" => "Segunda foto",
            ],
            [
                "nombre" => "1.jpg",
                "descripcion" => "Primera foto",
            ],
            [
                "nombre" => "2.jpg",
                "descripcion" => "Segunda foto",
            ],
            [
                "nombre" => "1.jpg",
                "descripcion" => "Primera foto",
            ],
            [
                "nombre" => "2.jpg",
                "descripcion" => "Segunda foto",
            ],
            [
                "nombre" => "1.jpg",
                "descripcion" => "Primera foto",
            ],
            [
                "nombre" => "2.jpg",
                "descripcion" => "Segunda foto",
            ],
            [
                "nombre" => "1.jpg",
                "descripcion" => "Primera foto",
            ],
            [
                "nombre" => "2.jpg",
                "descripcion" => "Segunda foto",
            ]
        ];
        
        

        return $this->render('listar', [
                    "imagenes" => $imagenes,
                    "titulo"=>"Las imagenes del proyecto",
                    "bloque2"=>"Estas son las imagenes del proyecto que son pasadas desde el controlador."
        ]);
    }

}
