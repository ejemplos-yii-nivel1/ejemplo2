<?php

use app\widgets\Miniaturas;

/* cargamos el primer bloque */
$this->beginBlock("Titulo");
echo $titulo;
$this->endBlock("Titulo");
/* fin del primer bloque */

/* cargamos el segundo bloque */
$this->beginBlock("Bloque2");
echo $bloque2;
$this->endBlock("Bloque2");
/* fin del segundo bloque */

echo Miniaturas::widget([
    "imagenes" => $imagenes
]);






