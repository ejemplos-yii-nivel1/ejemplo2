<p align="center">
    <h1 align="center">Ejemplo numero 2 de Yii - Nivel 1</h1>
    <br>
</p>

En este ejemplo vamos a partir del layout base y vamos a realizar en una serie de commits los siguientes cambios:
<ul>
<li>Crearemos dos controladores</li>
<li>Gestionar las vistas en estos controladores</li>
<li>Utilizar 1 layout diferente para cada controlador</li>
<li>Uno de los layouts va a utilizar bloques</li>
<li>Crearemos un widget basico para mostrar miniaturas de bootstrap</li>
<li>Utilizar widget creado</li>
</ul>

INSTALACION
------------

### INSTALANDO MEDIANTE COMPOSER

Teniendo Composer instalado, puedes instalar Yii ejecutando los siguientes 
comandos en un directorio accesible vía Web:

~~~
composer create-project ejemplos-yii-nivel1/layout1.git ejemplo2Nivel1
~~~

Puedes elegir un nombre de directorio diferente si así lo deseas.