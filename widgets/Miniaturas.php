<?php

namespace app\widgets;

use Yii;

class Miniaturas extends \yii\base\Widget {

    public $imagenes;

    public function run() {
        echo '<div class="row">';
        foreach($this->imagenes as $foto){
            echo $this->render("_miniaturas", [
                    "ruta" => Yii::getAlias("@web") . "/imgs/" . $foto["nombre"],
                    "descripcion" => $foto["descripcion"]
        ]);
        }
        echo "'</div>'";
        
    }

}
